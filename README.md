cube\_code
==========

Create GameCube (and I guess Wii also, though I haven't tried it on a Wii) REL files from ELF binaries.

## Usage

1. Compile some code for the `powerpc-none-eabi` target

Here's an example snippet, "hello.c".
```c
void OSReport(char *fmt, ...);

int its_a_global = 87;

int my_awesome_function(int a) {
    return a * 2;
}

void _prolog(void) {
    OSReport("Hello world from my REL %s %d", "uwu", 42);
    OSReport("Did you know 87 * 2 = %d", my_awesome_function(its_a_global));
}
```

This can be compiled as such to generate "hello.o"
```sh
clang -target powerpc-none-eabi -c hello.c
```

2. Create a symbol map to let the linker know where `OSReport` is. For Super Monkey Ball 2, the `OSReport` function is located in the static .dol file (hence the module and section IDs of zero). When loaded into memory, `OSReport` is located at `0x8000E6D0`, or `2147542736` in base 10.
```json
{
  "symbols": {
    "OSReport": { "module_id": 0, "section_id": 0, "offset": 2147542736 }
  }
}
```

3. Run `cube_code` to generate a REL file, "out.rel"
```sh
cube_code -vv link hello.o --module-id 255 --symbol-map smb2_symbol_map.json --output out.rel
```

Bam, you have a REL file now.

![The above code snippet running in Dolphin Emulator](https://i.imgur.com/1aC2R93.png)

## Stuff that doesn't work yet
- Uninitialized data (stuff that a linker would usually put in a `.bss` section) errors the tool for now
