use crate::error::*;
use crate::rel::{Rel, RelReloc, RelSectionOffsetted, RelSymbol, SymbolType, SymbolVisibility};
use crate::SourceLoc;
use goblin::elf;
use serde::{Serialize, Deserialize};
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

// const R_PPC_NONE: u32 = 0;
// const R_PPC_ADDR32: u32 = 1; // 32bit absolute address
// const R_PPC_ADDR24: u32 = 2; // 26bit address, 2 bits ignored.
// const R_PPC_ADDR16: u32 = 3; // 16bit absolute address
// const R_PPC_ADDR16_LO: u32 = 4; // lower 16bit of absolute address
// const R_PPC_ADDR16_HI: u32 = 5; // high 16bit of absolute address
// const R_PPC_ADDR16_HA: u32 = 6; // adjusted high 16bit
// const R_PPC_ADDR14: u32 = 7; // 16bit address, 2 bits ignored
// const R_PPC_ADDR14_BRTAKEN: u32 = 8;
// const R_PPC_ADDR14_BRNTAKEN: u32 = 9;
const R_PPC_REL24: u32 = 10; // PC relative 26 bit
// const R_PPC_REL14: u32 = 11; // PC relative 16 bit
// const R_PPC_REL14_BRTAKEN: u32 = 12;
// const R_PPC_REL14_BRNTAKEN: u32 = 13;
// const R_PPC_GOT16: u32 = 14;
// const R_PPC_GOT16_LO: u32 = 15;
// const R_PPC_GOT16_HI: u32 = 16;
// const R_PPC_GOT16_HA: u32 = 17;
const R_PPC_PLTREL24: u32 = 18;
// const R_PPC_COPY: u32 = 19;
// const R_PPC_GLOB_DAT: u32 = 20;
// const R_PPC_JMP_SLOT: u32 = 21;
// const R_PPC_RELATIVE: u32 = 22;
// const R_PPC_LOCAL24PC: u32 = 23;
// const R_PPC_UADDR32: u32 = 24;
// const R_PPC_UADDR16: u32 = 25;
const R_PPC_REL32: u32 = 26;
// const R_PPC_PLT32: u32 = 27;
// const R_PPC_PLTREL32: u32 = 28;
// const R_PPC_PLT16_LO: u32 = 29;
// const R_PPC_PLT16_HI: u32 = 30;
// const R_PPC_PLT16_HA: u32 = 31;
// const R_PPC_SDAREL16: u32 = 32;
// const R_PPC_SECTOFF: u32 = 33;
// const R_PPC_SECTOFF_LO: u32 = 4;
// const R_PPC_SECTOFF_HI: u32 = 5;
// const R_PPC_SECTOFF_HA: u32 = 6;

pub fn link_files(
    paths: &Vec<PathBuf>,
    module_id: u32,
    symbol_map_paths: &Vec<PathBuf>,
    output: &Path,
    output_map: &Option<PathBuf>,
) -> Result<()> {
    debug!("Loading symbol maps");
    // Load symbol maps
    let mut symbol_map = SymbolMap::new();
    for path in symbol_map_paths {
        trace!("Loading symbol map '{}'", path.display());
        let contents = fs::read(path)
            .chain_err(|| format!("Failed to read symbol map '{}'", path.display()))?;
        let map: SymbolMap = serde_json::from_slice(&contents)
            .chain_err(|| format!("Failed to load map '{}'", path.display()))?;
        symbol_map.merge_from(&map)?;
    }

    let mut rel = Rel::new();
    rel.module_id = module_id;

    let mut unresolved_symbols: Vec<UnresolvedSymbol> = Vec::new();
    let mut common_symbols = CommonSymbolMap::new();

    for path in paths {
        debug!("Adding binary '{}'", path.display());
        let contents = fs::read(path)?;
        let binary = goblin::elf::Elf::parse(&contents)?;

        let mut elf_section_id_remap = HashMap::<u32, RelSectionOffsetted>::new(); // Remap ELF section indices to REL section indices + offsets
        // Go through ELF sections, and add relevant ones to the REL
        // Also look for the bss section
        for (i, section) in binary.section_headers.iter().enumerate() {
            let name = match binary.shdr_strtab.get(section.sh_name) {
                Some(name) => name?,
                None => match binary.strtab.get(section.sh_name) {
                    Some(name) => name?,
                    None => bail!("ELF section name referenced string index {} which does not exist", section.sh_name),
                }
            };

            // Only sections that take up space in memory need to be put in a REL
            // (as well as the bss, which we do further down)
            if section.is_alloc() {
                let rel_section = rel.create_or_append_from_elf_section(section, &contents);
                elf_section_id_remap.insert(i as u32, rel_section);

                trace!("Keeping section {} '{}'", i, name);
            } else {
                trace!("Throwing out section {} '{}'", i, name);
            }

            if name == ".bss" {
                let offset = rel.add_size_to_bss_aligned(section.sh_size as _);
                elf_section_id_remap.insert(
                    i as u32,
                    RelSectionOffsetted {
                        section_idx: rel.get_or_create_bss_section() as _,
                        offset: offset,
                    }
                    );
            }
        }

        // Go through ELF relocations and add them to the REL
        // An ELF has sections containing various relocations (eg: .text.rela contains relocation info for .text)
        for (rela_section_idx, relocs) in &binary.shdr_relocs {
            for elf_reloc in relocs.iter() {
                // Conveniently, the first 13 relocation types are more or less the same between ELFs and RELs
                // And we shouldn't need to worry about the others for now
                let reloc_type = if elf_reloc.r_type <= 13 {
                    elf_reloc.r_type
                } else if elf_reloc.r_type == R_PPC_PLTREL24 { // An unresolved function
                    R_PPC_REL24
                } else if elf_reloc.r_type == R_PPC_REL32 {
                    // TODO: Kludge?
                    warn!("Found R_PPC_REL32");
                    R_PPC_REL24
                    // panic!("R_PPC_REL32 is not supported!")
                } else {
                    bail!("Invalid relocation type {}", elf_reloc.r_type)
                };

                let elf_from_section_sym = match binary.syms.get(elf_reloc.r_sym) {
                    Some(sym) => sym,
                    None => bail!("ELF relocation referenced symbol index {} which does not exist in the ELF symbol table", elf_reloc.r_sym),
                };
                let elf_from_section_idx = elf_from_section_sym.st_shndx as u32;

                let elf_rela_section = match binary.section_headers.get(*rela_section_idx) {
                    Some(elf_section) => elf_section,
                    None => bail!("ELF relocation referenced section index {} which does not exist in the ELF", rela_section_idx),
                };

                let elf_to_section = elf_rela_section.sh_info;
                let rel_to_section = match elf_section_id_remap.get(&elf_to_section) {
                    Some(section) => section,
                    None => {
                        warn!("ELF relocation referenced section index {} which does not exist or was discarded", elf_to_section);
                        continue
                    },
                };

                if elf_from_section_idx == goblin::elf::section_header::SHN_UNDEF {
                    // It's an unresolved symbol
                    let symbol_name = match binary.strtab.get(elf_from_section_sym.st_name) {
                        Some(name) => name,
                        None => bail!("ELF relocation referenced symbol index {} whose name does not exist in the ELF string table", elf_reloc.r_sym),
                    }?;

                    unresolved_symbols.push(UnresolvedSymbol {
                        to_section: rel_to_section.section_idx + 1, // +1 for the null section
                        to_offset: elf_reloc.r_offset as u32 + rel_to_section.offset,
                        from_symbol_name: String::from(symbol_name),
                        reloc_type: reloc_type as u8,
                        source_loc: SourceLoc::File(path.clone()),
                    });

                } else if elf_from_section_idx == goblin::elf::section_header::SHN_COMMON {
                    // We, as a linker, should create a .bss section with storage for common symbols ourselves
                    // OR, if the symbol resides within the data section of another object file, use that instead
                    let symbol_name = match binary.strtab.get(elf_from_section_sym.st_name) {
                        Some(name) => name,
                        None => bail!("ELF relocation referenced symbol index {} whose name does not exist in the ELF string table", elf_reloc.r_sym),
                    }?;

                    common_symbols.add_relocation(
                        String::from(symbol_name),
                        rel_to_section.section_idx + 1, // +1 for the null section
                        elf_reloc.r_offset as u32 + rel_to_section.offset,
                        reloc_type as u8,
                        elf_from_section_sym.st_size as u32,
                        elf_from_section_sym.st_value as u32, // Alignment is stored in a common symbol's "value"
                        );

                } else {
                    // We're relocating against ourself
                    let rel_from_section = match elf_section_id_remap.get(&elf_from_section_idx) {
                        Some(section) => section,
                        None => bail!("ELF relocation referenced section index {} which does not exist or was discarded", elf_from_section_idx),
                    };

                    let symbol_name = match binary.strtab.get(elf_from_section_sym.st_name) {
                        Some(name) => name,
                        None => bail!("ELF relocation referenced symbol index {} whose name does not exist in the ELF string table", elf_reloc.r_sym),
                    }?;

                    if symbol_name.is_empty() {
                        // Resolve the symbol right here
                        let rel_from_offset = match elf_reloc.r_addend {
                            Some(offset) => offset as u32,
                            None => bail!("ELF relocation does not have an addend"),
                        };

                        let rel_reloc = RelReloc {
                            to_section: rel_to_section.section_idx + 1, // +1 for the null section
                            to_offset: elf_reloc.r_offset as u32 + rel_to_section.offset,
                            from_section: rel_from_section.section_idx + 1, // +1 for the null section
                            from_offset: rel_from_offset + rel_from_section.offset,
                            reloc_type: reloc_type as u8,
                        };
                        rel.add_relocation(rel.module_id, rel_reloc);
                    } else {
                        trace!("Deferring relocation of internal symbol {}", symbol_name);
                        // Named symbols seem to have an r_addend of zero, so treat them as if they were undefined and
                        // resolve them later
                        unresolved_symbols.push(UnresolvedSymbol {
                            to_section: rel_to_section.section_idx + 1, // +1 for the null section
                            to_offset: elf_reloc.r_offset as u32 + rel_to_section.offset,
                            from_symbol_name: String::from(symbol_name),
                            reloc_type: reloc_type as u8,
                            source_loc: SourceLoc::File(path.clone())
                        });
                    }
                }
            }
        }

        // Static .bss symbols from an ELF get placed into the .bss section
        // We need to remap .bss offsets from ELF space to REL space
        // The key is each symbol's .bss offset in the ELF
        // The value is each symbol's .bss offset in the REL
        let mut local_bss_symbol_remap = HashMap::<u32, u32>::new();

        // Go through ELF symbols (symbols stored in the ELF, not dynamic symbols) and add them to the REL
        for elf_symbol in &binary.syms {
            let name = match binary.strtab.get(elf_symbol.st_name) {
                Some(name) => name?,
                None => bail!("ELF symbol name referenced string index {} which does not exist", elf_symbol.st_name),
            };

            let symbol_type = match elf_symbol.st_type() {
                elf::sym::STT_FUNC => SymbolType::Function,
                elf::sym::STT_OBJECT => SymbolType::Object,
                x => {
                    trace!("Throwing out symbol '{}' from '{}' with type {}", name, path.display(), x);
                    continue
                }
            };

            // Don't add local symbols
            // (Apart from static bss symbols)
            if elf_symbol.st_bind() == elf::sym::STB_LOCAL {
                trace!("Throwing out symbol '{}' with local bind", name);
                continue;
            }

            // Defer adding common symbols
            if elf_symbol.st_shndx as u32 == elf::section_header::SHN_COMMON {
                trace!("Defering adding common symbol '{}' with type {:?}", name, symbol_type);
                // Alignment is stored in a common symbol's "value"
                common_symbols.add_sym(String::from(name), elf_symbol.st_size as u32, elf_symbol.st_value as u32);
                continue;
            }

            // Check that the symbol doesn't already exist
            for symbol in &rel.symbols {
                if name == symbol.name {
                    bail!(
                        "Found duplicate symbol '{}'\n    First defined in {}\n    also defined in '{}'",
                        symbol.name,
                        symbol.source_loc,
                        path.display(),
                        );
                }
            }

            let rel_section = match elf_section_id_remap.get(&(elf_symbol.st_shndx as u32)) {
                Some(section) => section,
                None => bail!(
                    "ELF symbol '{}' referenced section index {} which does not exist or was discarded",
                    name,
                    elf_symbol.st_shndx
                ),
            };

            let visibility = match elf_symbol.st_bind() {
                elf::sym::STB_GLOBAL => SymbolVisibility::Global,
                elf::sym::STB_LOCAL => SymbolVisibility::Local,
                x => {
                    warn!("Unknown symbol visibility type {} for symbol '{}' - defaulting to local visibility", x, name);
                    SymbolVisibility::Local
                }
            };

            trace!("Adding symbol '{}' with type {:?}", name, symbol_type);
            rel.symbols.push(RelSymbol {
                name: name.to_string(),
                section: rel_section.section_idx,
                offset: elf_symbol.st_value as u32 + rel_section.offset,
                symbol_type,
                visibility,
                source_loc: SourceLoc::File(path.clone()),
            });
        }
    }

    // Resolve common symbols
    for (name, sym) in common_symbols.map {
        match symbol_map.symbols.get(&name) {
            Some(symbol_info) => {
                trace!("Resolved common external symbol '{}'", name);

                for reloc in &sym.relocations {
                    let rel_reloc = RelReloc {
                        to_section: reloc.to_section,
                        to_offset: reloc.to_offset,
                        from_section: symbol_info.section_id,
                        from_offset: symbol_info.offset,
                        reloc_type: reloc.reloc_type,
                    };
                    rel.add_relocation(symbol_info.module_id, rel_reloc);
                }
            }
            None => {
                match rel.get_symbol(&name) {
                    Some(rel_symbol) => {
                        trace!("Resolved common REL symbol '{}'", name);

                        let from_section = rel_symbol.section + 1; // +1 for the null section
                        let from_offset = rel_symbol.offset;

                        for reloc in &sym.relocations {
                            let rel_reloc = RelReloc {
                                to_section: reloc.to_section,
                                to_offset: reloc.to_offset,
                                from_section,
                                from_offset,
                                reloc_type: reloc.reloc_type,
                            };
                            rel.add_relocation(rel.module_id, rel_reloc);
                        }
                    }
                    None => {
                        trace!("Creating .bss storage for common symbol '{}'", name);

                        // Create the symbol in .bss
                        // TODO: Not assume global visibility
                        let bss_sym = (*rel.add_to_bss(name, sym.size, sym.align, SymbolVisibility::Global)).clone();

                        for reloc in &sym.relocations {
                            let rel_reloc = RelReloc {
                                to_section: reloc.to_section,
                                to_offset: reloc.to_offset,
                                from_section: bss_sym.section + 1, // +1 for the null section
                                from_offset: bss_sym.offset,
                                reloc_type: reloc.reloc_type,
                            };
                            rel.add_relocation(rel.module_id, rel_reloc);
                        }
                    },
                }
            }
        }
    }

    // Resolve unresolved symbols
    let mut errors: Vec<Error> = Vec::new();
    for unresolved in unresolved_symbols {
        match symbol_map.symbols.get(&unresolved.from_symbol_name) {
            Some(symbol_info) => {
                trace!("Resolved external symbol '{}'", unresolved.from_symbol_name);

                let rel_reloc = RelReloc {
                    to_section: unresolved.to_section,
                    to_offset: unresolved.to_offset,
                    from_section: symbol_info.section_id,
                    from_offset: symbol_info.offset,
                    reloc_type: unresolved.reloc_type,
                };
                rel.add_relocation(symbol_info.module_id, rel_reloc);
            }
            None => {
                match rel.get_symbol(&unresolved.from_symbol_name) {
                    Some(rel_symbol) => {
                        trace!("Resolved REL symbol '{}'", unresolved.from_symbol_name);

                        let rel_reloc = RelReloc {
                            to_section: unresolved.to_section,
                            to_offset: unresolved.to_offset,
                            from_section: rel_symbol.section + 1, // +1 for the null section
                            from_offset: rel_symbol.offset,
                            reloc_type: unresolved.reloc_type,
                        };
                        rel.add_relocation(rel.module_id, rel_reloc);
                    }
                    None => {
                        errors.push(
                            format!(
                                "Could not resolve symbol '{}' from {}",
                                unresolved.from_symbol_name,
                                unresolved.source_loc,
                                ).into()
                            );
                    },
                }
            }
        }
    }

    if errors.len() > 0 {
        for error in &errors {
            error!("{}", error);
        }
        bail!("Encountered {} errors", errors.len());
    }

    // Look for special _prolog/_epilog/_unresolved symbols
    for symbol in &rel.symbols {
        match symbol.name.as_str() {
            "_prolog" => {
                if symbol.symbol_type != SymbolType::Function {
                    bail!("Found symbol '_prolog' but it is of type {:?}, not {:?}", symbol.symbol_type, SymbolType::Function);
                }

                trace!("Found _prolog function");
                rel.prolog_section = Some(symbol.section);
                rel.prolog_offset = symbol.offset;
            }
            "_epilog" => {
                if symbol.symbol_type != SymbolType::Function {
                    bail!("Found symbol '_epilog' but it is of type {:?}, not {:?}", symbol.symbol_type, SymbolType::Function);
                }

                trace!("Found _epilog function");
                rel.epilog_section = Some(symbol.section);
                rel.epilog_offset = symbol.offset;
            }
            "_unresolved" => {
                if symbol.symbol_type != SymbolType::Function {
                    bail!("Found symbol '_unresolved' but it is of type {:?}, not {:?}", symbol.symbol_type, SymbolType::Function);
                }

                trace!("Found _unresolved function");
                rel.unresolved_section = Some(symbol.section);
                rel.unresolved_offset = symbol.offset;
            }
            _ => {}
        }
    }

    // Write the output file
    debug!("Generating REL image");
    let image = rel.generate_image();

    debug!("Writing out REL image");
    let mut out_file = File::create(output)?;
    out_file.write_all(&image)?;
    debug!("Finished writing out REL image");

    match output_map {
        Some(output_map) => {
            debug!("Generating symbol map for REL");

            let mut map = SymbolMap::new();
            for symbol in &rel.symbols {
                if symbol.visibility != SymbolVisibility::Global { continue; }

                // Don't add the special prolog/epilog/unresolved functions to the map
                match symbol.name.as_str() {
                    "_prolog" => continue,
                    "_epilog" => continue,
                    "_unresolved" => continue,
                    _ => {}
                }

                trace!("Adding symbol '{}' to output symbol map", symbol.name);
                map.symbols.insert(symbol.name.clone(), SymbolInfo {
                    module_id: rel.module_id,
                    section_id: symbol.section + 1, // +1 for the null section
                    offset: symbol.offset,
                });
            }

            let map_json = serde_json::to_string(&map)
                .chain_err(|| "Failed to generate output symbol map")?;
            fs::write(output_map, map_json)
                .chain_err(|| format!("Failed to write output symbol map to '{}'", output_map.display()))?;

            debug!("Finished writing out symbol map for REL");
        }
        None => {}
    }

    Ok(())
}

#[derive(Serialize, Deserialize)]
struct SymbolMap {
    symbols: HashMap<String, SymbolInfo>,
}

impl SymbolMap {
    fn new() -> SymbolMap {
        SymbolMap {
            symbols: HashMap::new(),
        }
    }

    fn merge_from(&mut self, other: &SymbolMap) -> Result<()> {
        for (k, v) in &other.symbols {
            let entry = self.symbols.entry(k.clone());
            match entry {
                Entry::Occupied(_) => { bail!("Found duplicate symbol '{}' when merging symbol maps", k) }
                Entry::Vacant(e) => { e.insert(v.clone()); }
            }
        }

        Ok(())
    }
}

#[derive(Serialize, Deserialize, Clone)]
struct SymbolInfo {
    module_id: u32,
    section_id: u8,
    offset: u32,
}

struct UnresolvedSymbol {
    to_section: u8,
    to_offset: u32,
    from_symbol_name: String,
    reloc_type: u8,
    source_loc: SourceLoc,
}

struct CommonSymbolRel {
    to_section: u8,
    to_offset: u32,
    reloc_type: u8,
}

struct CommonSymbol {
    relocations: Vec<CommonSymbolRel>,
    size: u32,
    align: u32,
}

struct CommonSymbolMap {
    map: HashMap<String, CommonSymbol>,
}

impl CommonSymbolMap {
    fn new() -> CommonSymbolMap {
        CommonSymbolMap {
            map: HashMap::new(),
        }
    }

    fn add_sym(
        &mut self,
        symbol_name: String,
        size: u32,
        align: u32,
        ) {
        let mut sym = self.map.entry(symbol_name).or_insert(CommonSymbol {
            relocations: Vec::new(),
            size,
            align,
        });

        // Common symbols have the size of the biggest sym
        if sym.size < size {
            sym.size = size;
        }

        // I'm guessing the same about alignment too
        if sym.align < align {
            sym.align = align;
        }
    }

    fn add_relocation(
        &mut self,
        symbol_name: String,
        to_section: u8,
        to_offset: u32,
        reloc_type: u8,
        size: u32,
        align: u32,
    ) {
        let mut sym = self.map.entry(symbol_name).or_insert(CommonSymbol {
            relocations: Vec::new(),
            size,
            align,
        });

        // Common symbols have the size of the biggest sym
        if sym.size < size {
            sym.size = size;
        }

        // I'm guessing the same about alignment too
        if sym.align < align {
            sym.align = align;
        }

        sym.relocations.push(
            CommonSymbolRel {
                to_section,
                to_offset,
                reloc_type,
            }
        );
    }
}
