use cube_code::error::*;
use env_logger::Env;
use clap::{Arg, App, SubCommand};
use byteorder::{BigEndian, ReadBytesExt};
use std::path::{Path, PathBuf};
use std::fs::File;
use std::io::{Write, Seek, SeekFrom};
use std::env;

#[macro_use]
extern crate log;

#[macro_use]
extern crate clap;

#[macro_use]
extern crate error_chain;

fn main() {
    // Error handling
    if let Err(ref e) = run() {
        eprintln!("Error: {}", e);

        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }

        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace:\n{:?}", backtrace);
        } else {
            eprintln!("No backtrace available - run with `RUST_BACKTRACE=1` to enable backtraces on error");
        }

        eprintln!("Goodbye");
        std::process::exit(1);
    }
}

fn run() -> Result<()> {
    let matches = App::new("rel_tool")
        .version(crate_version!())
        .author("CraftedCart")
        .about("Tools to work with GameCube files")
        .arg(Arg::with_name("v")
             .short("v")
             .multiple(true)
             .help("Sets the level of verbosity (-v for debug logs, -vv for trace logs)"))
        .subcommand(SubCommand::with_name("link")
                    .about("Create a .rel file out of ELF .o files")
                    .version(crate_version!())
                    .author("CraftedCart")
                    .arg(Arg::with_name("OUTPUT")
                         .short("o")
                         .long("output")
                         .help("Sets the output file")
                         .takes_value(true)
                         .required(true))
                    .arg(Arg::with_name("MODULE-ID")
                         .short("i")
                         .long("module-id")
                         .help("Sets the output REL's module ID")
                         .takes_value(true)
                         .required(true))
                    .arg(Arg::with_name("SYMBOL-MAP")
                         .short("s")
                         .long("symbol-map")
                         .help("A JSON file describing symbol locations in other modules")
                         .takes_value(true)
                         .multiple(true))
                    .arg(Arg::with_name("EMIT-SYMBOL-MAP")
                         .short("S")
                         .long("emit-symbol-map")
                         .help("If specified, a symbol map for the REL will be emitted")
                         .takes_value(true))
                    .arg(Arg::with_name("INPUT")
                         .required(true)
                         .min_values(1)))
        .subcommand(SubCommand::with_name("info")
                    .about("Get info on a REL file (and its accompanying symbol map, if one is provided)")
                    .version(crate_version!())
                    .author("CraftedCart")
                    .arg(Arg::with_name("INPUT")
                         .required(true)
                         .min_values(1)))
                    // .arg(Arg::with_name("SYMBOL-MAP")
                         // .short("s")
                         // .long("symbol-map")
                         // .takes_value(true)))
        .subcommand(SubCommand::with_name("hello_world")
                    .about("Generate a simple Hello, world! REL file")
                    .version(crate_version!())
                    .author("CraftedCart")
                    .arg(Arg::with_name("OUTPUT")
                         .required(true)))
        .get_matches();

    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v')
    let verbosity_level = match matches.occurrences_of("v") {
        0 => "info",
        1 => "debug",
        2 | _ => "trace",
    };
    env_logger::from_env(Env::default().default_filter_or(verbosity_level)).init();
    if matches.occurrences_of("v") > 2 { warn!("Don't be crazy, -vv is enough for trace level logging"); }

    match matches.subcommand() {
        ("link", Some(sub_matches)) => {
            let cwd = env::current_dir()?;
            let inputs = sub_matches.values_of("INPUT")
                .unwrap()
                .map(|val| cwd.join(PathBuf::from(val)))
                .collect();
            let symbol_maps = sub_matches.values_of("SYMBOL-MAP")
                .unwrap()
                .map(|val| cwd.join(PathBuf::from(val)))
                .collect();
            let output = sub_matches.value_of("OUTPUT").unwrap();
            let output_map = match sub_matches.value_of("EMIT-SYMBOL-MAP") {
                Some(path) => Some(cwd.join(path)),
                None => None,
            };

            let module_id = match sub_matches.value_of("MODULE-ID").unwrap().parse::<u32>() {
                Ok(id) => id,
                Err(_) => bail!("MODULE-ID is not a value u32 number"),
            };

            cube_code::link::link_files(&inputs, module_id, &symbol_maps, &cwd.join(output), &output_map)?;
        }
        ("info", Some(sub_matches)) => {
            let cwd = env::current_dir()?;
            let input = sub_matches.value_of("INPUT").unwrap();
            // let symbol_map = sub_matches.value_of("SYMBOL-MAP");

            dump_rel(
                &cwd.join(input),
                // &symbol_map.map(|map| cwd.join(map))
                )?;
        }
        ("hello_world", Some(sub_matches)) => {
            let cwd = env::current_dir()?;
            let output = sub_matches.value_of("OUTPUT").unwrap();
            write_hello_world(&cwd.join(output))?;
        }
        _ => {
            error!("No subcommand given");
            error!("For more information try --help");
            std::process::exit(1);
        }
    }

    Ok(())
}

// fn dump_rel(rel_path: &Path, sym_map: &Option<PathBuf>) -> Result<()> {
fn dump_rel(rel_path: &Path) -> Result<()> {
    let mut f = File::open(rel_path)?;

    println!("REL HEADER");
    println!("==============================================");
    println!("ITEM                              | VALUE");
    println!("----------------------------------+-----------");
    println!("Rel ID                            | {}"      , f.read_u32::<BigEndian>()?);
    println!("Next mod ptr (should be null)     | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("Prev mod ptr (should be null)     | {:#010x}", f.read_u32::<BigEndian>()?);
    let section_count = f.read_u32::<BigEndian>()?;
    println!("Section count                     | {}"      , section_count);
    let section_table_offset = f.read_u32::<BigEndian>()?;
    println!("Section table offset              | {:#010x}", section_table_offset);
    println!("Module name offset                | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("Module name size                  | {:#010x}", f.read_u32::<BigEndian>()?);
    let rel_version = f.read_u32::<BigEndian>()?;
    println!("REL version                       | {}"      , rel_version);
    println!(".bss size                         | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("Relocation table offset           | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("Imp table offset                  | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("Imp table size                    | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("_prolog function section          | {}"      , f.read_u8()?);
    println!("_epilog function section          | {}"      , f.read_u8()?);
    println!("_unresolved function section      | {}"      , f.read_u8()?);
    println!(".bss section (should be null)     | {}"      , f.read_u8()?);
    println!("_prolog function offset           | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("_epilog function offset           | {:#010x}", f.read_u32::<BigEndian>()?);
    println!("_unresolved function offset       | {:#010x}", f.read_u32::<BigEndian>()?);

    if rel_version >= 2 {
        println!("All sections alignment constraint | {}"      , f.read_u32::<BigEndian>()?);
        println!(".bss alignment constraint         | {}"      , f.read_u32::<BigEndian>()?);
    }

    if rel_version >= 3 {
        println!("Fix size                          | {:#010x}", f.read_u32::<BigEndian>()?);
    }

    println!();
    f.seek(SeekFrom::Start(section_table_offset as _))?;
    println!("SECTION TABLE");
    println!("=======================================");
    println!("OFFSET     | UNK   | EXEC  | LENGTH");
    println!("-----------+-------+-------+-----------");

    for _ in 0..section_count {
        let offset_unk_exec = f.read_u32::<BigEndian>()?;
        let unk = (offset_unk_exec & 2) != 0;
        let exec = (offset_unk_exec & 1) != 0;
        let offset = offset_unk_exec & !3;
        let length = f.read_u32::<BigEndian>()?;
        println!("{:#010x} | {: <5} | {: <5} | {:#010x}", offset, unk, exec, length);
    }

    Ok(())
}

/// This is a simple REL containing a function that calls OSReport("Hello, world!");. This function is set as the REL
/// prolog, so it is called right after loading/linking the REL into memory.
///
/// This doesn't save/restore the link register, so returning from the function gets stuck in an infinite loop of
/// returning to itself
fn write_hello_world(path: &Path) -> Result<()> {
    let mut file = File::create(path)?;
    file.write_all(&[
        //////////////// REL HEADER @ 0x00000000 ////////////////
        0x00, 0x00, 0x00, 0xFF, // ID
        0x00, 0x00, 0x00, 0x00, // Next module pointer (Runtime filled)
        0x00, 0x00, 0x00, 0x00, // Prev module pointer (Runtime filled)
        0x00, 0x00, 0x00, 0x03, // Number of sections
        0x00, 0x00, 0x00, 0x48, // Offset to section table
        0x00, 0x00, 0x00, 0x00, // Offset to module name string (Nullable)
        0x00, 0x00, 0x00, 0x00, // Size of module name in bytes
        0x00, 0x00, 0x00, 0x02, // REL version (v2 in this case)
        0x00, 0x00, 0x00, 0x00, // Size of .bss
        0x00, 0x00, 0x00, 0x80, // Offset to the relocation table
        0x00, 0x00, 0x00, 0xB8, // Offset to the imp table
        0x00, 0x00, 0x00, 0x10, // Size of the imp table in bytes
        0x01, // Index into section table which prolog is relative to. Skip if this field is 0.
        0x00, // Index into section table which epilog is relative to. Skip if this field is 0.
        0x00, // Index into section table which unresolved is relative to. Skip if this field is 0.
        0x00, // Index into section table which bss is relative to. (Runtime filled)
        0x00, 0x00, 0x00, 0x00, // Offset of _prolog from the specified section
        0x00, 0x00, 0x00, 0x00, // Offset of _epilog from the specified section
        0x00, 0x00, 0x00, 0x00, // Offset of _unresolved from the specified section
        0x00, 0x00, 0x00, 0x20, // Alignment constraint on all sections (v2+ RELs only)
        0x00, 0x00, 0x00, 0x20, // Alignment constraint on .bss section (v2+ RELs only)
        // 0x00, 0x00, 0x00, 0x20, // Fix size, (v3+ RELs only)

        //////////////// SECTION INFO TABLE @ 0x00000048 LEN 0x00000018 ////////////////
        // Nothing!
        0x00, 0x00, 0x00, 0x00, // Offset to section (div 4) + unk flag + executable flag
        0x00, 0x00, 0x00, 0x00, // Length of section

        // .text0 000000000000000000000000011000 0 1
        0x00, 0x00, 0x00, 0x61, // Offset to section (div 4) + unk flag + executable flag
        0x00, 0x00, 0x00, 0x10, // Length of section

        // .data0 000000000000000000000000011100 0 0
        0x00, 0x00, 0x00, 0x70, // Offset to section (div 4) + unk flag + executable flag
        0x00, 0x00, 0x00, 0x10, // Length of section

        //////////////// .TEXT0 SECTION (Index 1) @ 0x00000060 LEN 0x00000010 ////////////////
        0x3C, 0x60, 0x00, 0x00, // lis r3, 0x0000 (To be relocated to load the higher bits of the hello world string addr)
        0x60, 0x63, 0x00, 0x00, // ori r3, r3, 0x0000 (To be relocated to load the lower bits of the hello world string addr)
        0x48, 0x00, 0x00, 0x01, // bl 0x00000000 (To be relocated to use the OSReport func addr)
        0x4E, 0x80, 0x00, 0x20, // blr

        //////////////// .DATA0 SECTION (Index 2) @ 0x00000070 LEN 0x00000010 ////////////////
        0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x21, 0x00, 0x00, 0x00, // "Hello, world!\0"

        //////////////// RELOCATION TABLE @ 0x00000080 LEN 0x00000038 ////////////////
        //// main.dol ////
        0x00, 0x00, // Offset from the previous relocation (or start of section if this is the first) to this one
        0xCA, // Relocation type (0xCA = R_DOLPHIN_SECTION, change the section relocations will be applied to)
        0x01, // Section of the symbol to relocate against (or in this case, the section we want to apply relocations too)
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against (Not applicable here)

        0x00, 0x08, // Offset from the previous relocation (or start of section if this is the first) to this one
        0x0A, // Relocation type (0x0A = R_PPC_REL24, for relocating relative branch instructions)
        0x04, // Section of the symbol to relocate against
        0x80, 0x00, 0xE6, 0xD0, // Offset of the symbol to relocate against (OSReport)

        0x00, 0x00, // Offset from the previous relocation (or start of section if this is the first) to this one
        0xCB, // Relocation type (0xCB = stop parsing)
        0x00, // Section of the symbol to relocate against
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against

        //// This REL ////
        0x00, 0x00, // Offset from the previous relocation (or start of section if this is the first) to this one
        0xCA, // Relocation type (0xCA = R_DOLPHIN_SECTION, change the section relocations will be applied to)
        0x01, // Section of the symbol to relocate against (or in this case, the section we want to apply relocations to)
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against (Not applicable here)

        0x00, 0x02, // Offset from the previous relocation (or start of section if this is the first) to this one
        0x06, // Relocation type (0x06 = R_PPC_ADDR16_HA , write high bits of symbol + 0x8000)
        0x02, // Section of the symbol to relocate against
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against

        0x00, 0x04, // Offset from the previous relocation (or start of section if this is the first) to this one
        0x04, // Relocation type (0x06 = R_PPC_ADDR16_LO , write low bits of symbol)
        0x02, // Section of the symbol to relocate against
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against

        0x00, 0x00, // Offset from the previous relocation (or start of section if this is the first) to this one
        0xCB, // Relocation type (0xCB = stop parsing)
        0x00, // Section of the symbol to relocate against
        0x00, 0x00, 0x00, 0x00, // Offset of the symbol to relocate against

        //////////////// IMP TABLE @ 0x000000B8 LEN 0x00000010 ////////////////
        0x00, 0x00, 0x00, 0x00, // ID of the REL that the relocations refer to (0x00 = main.dol)
        0x00, 0x00, 0x00, 0x80, // Offset to relocation table

        0x00, 0x00, 0x00, 0xFF, // ID of the REL that the relocations refer to (0xFF is this REL's ID)
        0x00, 0x00, 0x00, 0x98, // Offset to relocation table
    ])?;

    Ok(())
}
