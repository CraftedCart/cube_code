use byteorder::{BigEndian, WriteBytesExt};
use goblin::elf::section_header::SectionHeader;
use crate::SourceLoc;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::io::Write;

const REL_V1_HEADER_SIZE: u32 = 0x40;
const REL_V2_HEADER_SIZE: u32 = 0x48;
const REL_V3_HEADER_SIZE: u32 = 0x4C;
const REL_SECTION_INFO_SIZE: u32 = 0x8;
const REL_RELOCATION_SIZE: u32 = 0x8;
const REL_IMP_SIZE: u32 = 0x8;

// Relocation types
const R_PPC_NONE: u8 = 0; // Do nothing. Skip this entry.
// const R_PPC_ADDR32: u8 = 1; // Write the 32 bit address of the symbol.
// const R_PPC_ADDR24: u8 = 2; // Write the 24 bit address of the symbol divided by four shifted up 2 bits to the 32 bit value (for relocating branch instructions). Fail if address won't fit.
// const R_PPC_ADDR16: u8 = 3; // Write the 16 bit address of the symbol. Fail if address more than 16 bits.
// const R_PPC_ADDR16_LO: u8 = 4; // Write the low 16 bits of the address of the symbol.
// const R_PPC_ADDR16_HI: u8 = 5; // Write the high 16 bits of the address of the symbol.
// const R_PPC_ADDR16_HA: u8 = 6; // Write the high 16 bits of the address of the symbol plus 0x8000.
// const R_PPC_ADDR14: u8 = 7; // Write the 14 bits of the address of the symbol divided by four shifted up 2 bits to the 32 bit value (for relocating conditional branch instructions). Fail if address won't fit.
// const R_PPC_ADDR14_2: u8 = 8; // Same as above
// const R_PPC_ADDR14_3: u8 = 9; // Same as above
// const R_PPC_REL24: u8 = 10; // Write the 24 bit address of the symbol minus the address of the relocation divided by four shifted up 2 bits to the 32 bit value (for relocating relative branch instructions). Fail if address won't fit.
// const R_PPC_REL14: u8 = 11; // Write the 14 bit address of the symbol minus the address of the relocation divided by four shifted up 2 bits to the 32 bit value (for relocating conditional relative branch instructions). Fail if address won't fit.
// const R_PPC_REL14_2: u8 = 12; // Same as above
// const R_PPC_REL14_3: u8 = 13; // Same as above
// const R_UNK1: u8 = 14; // Undefined, handled like 203 R_RVL_STOP by newer Nintendo SDKs.
// const R_DOLPHIN_NOP: u8 = 201; // Do nothing. Skip this entry. Carry the address of the symbol to the next entry.
// const R_RVL_NONE: u8 = 201; // Same as above
const R_DOLPHIN_SECTION: u8 = 202; // Change which section relocations are being applied to. Set the offset into the section to 0.
// const R_RVL_SECT: u8 = 202; // Same as above
const R_DOLPHIN_END: u8 = 203; // Stop parsing the relocation table.
// const R_RVL_STOP: u8 = 203; // Same as above
// const R_DOLPHIN_MRKREF: u8 = 204; // Unknown.

/// Information about a relocatable object for the Nintendo GameCube or Wii
pub struct Rel {
    /// A unique ID number for the REL. This must be unique across all RELs in a game, and cannot be zero (as module ID
    /// zero denotes the static portion, main.dol).
    pub module_id: u32,

    /// A Vec of sections in the REL. A REL may have multiple sections, such as `.text` for executable code and
    /// `.data` for variable and constant storage.
    pub sections: Vec<RelSection>,

    /// An optional name for this module. Should only contain ASCII characters.
    pub module_name: Option<String>,

    /// The version number for the REL
    pub rel_version: u32,

    /// The size of the `.bss` (uninitialized data) section
    pub bss_size: u32,

    /// The section index of the .bss section
    bss_section: Option<usize>,

    /// Relocation data for the REL.
    ///
    /// As RELs may be loaded anywhere in memory, and the load location is unspecified, certain parts of RELs must be
    /// patched when they are loaded (Eg: a `bl magic_function` instruction in this REL, where `magic_function` lives in
    /// another REL, means that this REL should have that instruction patched to use the location of where
    /// `magic_function` was loaded in memory; this is called runtime linking, and is performed by the `OSLink` function
    /// on the GameCube).
    ///
    /// The key of this HashMap is the module ID that relocations should relocate against (module ID 0 refers to the
    /// static portion, main.dol).
    ///
    /// The value of this HashMap is a Vec of relocation data, a list of what parts of this REL should be patched with
    /// the location of symbols from another REL/main.dol.
    pub relocations: HashMap<u32, Vec<RelReloc>>,

    /// The alignment constraint on all sections. Only applies to version 2+ RELs.
    pub section_alignment: u32,

    /// The alignment constraint on the `.bss` (uninitialized data) section. Only applies to version 2+ RELs.
    pub bss_alignment: u32,

    /// If REL is linked with `OSLinkFixed` (instead of `OSLink`), the space after this address can be used for other
    /// purposes (like bss). Only applies to version 3+ RELs.
    pub fixed_size: u32,

    /// A list of symbols that reside within this REL's sections. A REL image itself doesn't store any kind of symbol
    /// table, this is here just to help with linking.
    pub symbols: Vec<RelSymbol>,

    /// The section of this REL's `_prolog` function, which is called when the REL is loaded in to memory.
    pub prolog_section: Option<u8>,

    /// Where the prolog function is relative to the start of the prolog's section. This can be zero if no prolog is
    /// present.
    pub prolog_offset: u32,

    /// The section of this REL's `_epilog` function, which is called before the REL is unloaded from memory.
    pub epilog_section: Option<u8>,

    /// Where the epilog function is relative to the start of the epilog's section. This can be zero if no epilog is
    /// present.
    pub epilog_offset: u32,

    /// The section of this REL's `_unresolved` function, which is called when the REL tries to call an unlinked
    /// function.
    pub unresolved_section: Option<u8>,

    /// Where the unresolved function is relative to the start of the unresolved's section. This can be zero if no
    /// unresolved is present.
    pub unresolved_offset: u32,
}

impl Rel {
    /// Create a new REL struct with no sections, relocations, or symbols
    pub fn new() -> Rel {
        Rel {
            module_id: 1,
            sections: Vec::new(),
            module_name: None,
            rel_version: 2,
            bss_size: 0,
            bss_section: None,
            relocations: HashMap::new(),
            section_alignment: 32,
            bss_alignment: 32,
            fixed_size: 0,
            symbols: Vec::new(),
            prolog_section: None,
            prolog_offset: 0,
            epilog_section: None,

            epilog_offset: 0,
            unresolved_section: None,
            unresolved_offset: 0,
        }
    }

    /// Add a relocation to this REL
    pub fn add_relocation(&mut self, other_module_id: u32, reloc: RelReloc) {
        // Get the relocations vec corresponding to the other_module_id
        // If the vec doesn't exist, create one and insert it
        let reloc_vec = self
            .relocations
            .entry(other_module_id)
            .or_insert_with(|| Vec::new());

        reloc_vec.push(reloc);
    }

    pub fn create_or_append_from_elf_section(
        &mut self,
        elf_section: &SectionHeader,
        elf_contents: &[u8],
    ) -> RelSectionOffsetted {
        let section_start = elf_section.sh_offset as usize;
        let section_end = section_start + elf_section.sh_size as usize;

        let unk_flag = false;
        let execute_flag = elf_section.is_executable();

        let section_idx = self.find_section_with_attributes(unk_flag, execute_flag);
        match section_idx {
            Some(section_idx) => {
                let section = self.sections.get_mut(section_idx).unwrap();
                let offset = section.data.len() as u32;

                let desired_offset = round_up(offset, self.section_alignment);
                let padding_needed = desired_offset - offset;
                for _ in 0..padding_needed { section.data.write_u8(0).unwrap(); }
                let offset = section.data.len() as u32;

                section.data.extend_from_slice(&elf_contents[section_start..section_end]);

                RelSectionOffsetted {
                    section_idx: section_idx as u8,
                    offset: offset,
                }
            }
            None => {
                let section = RelSection {
                    data: Vec::from(&elf_contents[section_start..section_end]),
                    unk_flag: false,
                    execute_flag: elf_section.is_executable(),
                };

                self.sections.push(section);
                RelSectionOffsetted {
                    section_idx: self.sections.len() as u8 - 1,
                    offset: 0,
                }
            }
        }
    }

    pub fn add_to_bss(&mut self, name: String, size: u32, align: u32, visibility: SymbolVisibility) -> &RelSymbol {
        // TODO: Respect alignment constraints
        let offset = round_up(self.bss_size, align);

        let sym = RelSymbol {
            name,
            section: self.get_or_create_bss_section() as u8,
            offset: offset,
            symbol_type: SymbolType::Object,
            visibility,
            source_loc: SourceLoc::CommonSym,
        };
        self.symbols.push(sym);

        self.bss_size = offset + size;

        self.symbols.last().unwrap()
    }

    /// Returns the beginning offset
    pub fn add_size_to_bss_aligned(&mut self, size: u32) -> u32 {
        let offset = round_up(self.bss_size, 32); // Align to 32
        self.bss_size = offset + size;

        offset
    }

    pub fn get_symbol(&self, name: &str) -> Option<&RelSymbol> {
        for symbol in &self.symbols {
            if symbol.name == name {
                return Some(symbol);
            }
        }

        None
    }

    fn find_section_with_attributes(&self, unk_flag: bool, execute_flag: bool) -> Option<usize> {
        for (i, section) in self.sections.iter().enumerate() {
            if section.unk_flag == unk_flag && section.execute_flag == execute_flag {
                return Some(i);
            }
        }

        None
    }

    pub fn get_or_create_bss_section(&mut self) -> usize {
        match self.bss_section {
            Some(idx) => idx,
            None => {
                let section = RelSection {
                    data: Vec::with_capacity(0),
                    unk_flag: false,
                    execute_flag: false,
                };
                self.sections.push(section);

                let idx = self.sections.len() - 1;
                self.bss_section = Some(idx);

                idx
            }
        }
    }

    /// Generate a REL image from the struct. This image can be written out straight to a .rel file and loaded on a
    /// GameCube.
    pub fn generate_image(&self) -> Vec<u8> {
        debug!("Converting relocations to a format suitable for REL images");
        let relocations = self.convert_relocations_for_image();

        debug!("Calculating offsets for REL image");
        let num_sections = self.sections.len() as u32 + 1; // +1 as we start with a null section
        let section_info_table_start = match self.rel_version {
            1 => REL_V1_HEADER_SIZE,
            2 => REL_V2_HEADER_SIZE,
            3 => REL_V3_HEADER_SIZE,
            v => panic!("Invalid REL version {} when running Rel::generate_image(). Only versions 1, 2, and 3 are acceptable.", v),
        };
        let section_info_table_size = num_sections * REL_SECTION_INFO_SIZE;

        let section_data_start = section_info_table_start + section_info_table_size;
        // Key: Section ID, Value: Offset in image
        let mut section_offsets: HashMap<u32, u32> = HashMap::with_capacity(self.sections.len());
        let mut cur_offset = section_data_start;
        for (i, section) in self.sections.iter().enumerate() {
            cur_offset = round_up(cur_offset, self.section_alignment);
            section_offsets.insert(i as u32, cur_offset);
            cur_offset += section.data.len() as u32;
        }
        cur_offset = round_up(cur_offset, 4);
        let section_data_end = cur_offset;

        // Key: Module ID, Value: Offset in image
        let mut relocation_table_offsets: HashMap<u32, u32> = HashMap::with_capacity(self.relocations.len());
        let relocation_table_start: u32 = section_data_end;
        let mut cur_offset: u32 = section_data_end;
        for (module_id, relocs) in &relocations {
            relocation_table_offsets.insert(*module_id, cur_offset);
            cur_offset += relocs.len() as u32 * REL_RELOCATION_SIZE;
        }
        let relocation_table_end = cur_offset;

        let imp_table_start = relocation_table_end;
        let imp_table_size = relocations.len() as u32 * REL_IMP_SIZE;

        let mut image: Vec<u8> = Vec::new();

        //////////////// Write REL header ////////////////
        debug!("Adding header to REL image");
        let real_prolog_section = match self.prolog_section {
            Some(idx) => idx + 1, // +1 to account for the null section
            None => 0,
        };
        let real_epilog_section = match self.epilog_section {
            Some(idx) => idx + 1,
            None => 0,
        };
        let real_unresolved_section = match self.unresolved_section {
            Some(idx) => idx + 1,
            None => 0,
        };

        image.write_u32::<BigEndian>(self.module_id).unwrap(); // Module ID
        image.write_u32::<BigEndian>(0).unwrap(); // Next module ID (Runtime filled)
        image.write_u32::<BigEndian>(0).unwrap(); // Prev module ID (Runtime filled)
        image.write_u32::<BigEndian>(num_sections).unwrap(); // Number of sections
        image.write_u32::<BigEndian>(section_info_table_start).unwrap(); // Offset to section table
        image.write_u32::<BigEndian>(0).unwrap(); // Offset to module name string (Nullable) TODO
        image.write_u32::<BigEndian>(0).unwrap(); // Size of module name string TODO
        image.write_u32::<BigEndian>(self.rel_version).unwrap(); // REL version
        image.write_u32::<BigEndian>(self.bss_size).unwrap(); // Size of .bss
        image.write_u32::<BigEndian>(relocation_table_start).unwrap(); // Offset to relocation table
        image.write_u32::<BigEndian>(imp_table_start).unwrap(); // Offset to imp table
        image.write_u32::<BigEndian>(imp_table_size).unwrap(); // Size of imp table
        image.write_u8(real_prolog_section).unwrap(); // Index into section table which prolog is relative to (Nullable)
        image.write_u8(real_epilog_section).unwrap(); // Index into section table which epilog is relative to (Nullable)
        image.write_u8(real_unresolved_section).unwrap(); // Index into section table which unresolved is relative to (Nullable)
        image.write_u8(0).unwrap(); // Index into section table which bss is relative to (Runtime filled)
        image.write_u32::<BigEndian>(self.prolog_offset).unwrap(); // Offset of _prolog from specified section
        image.write_u32::<BigEndian>(self.epilog_offset).unwrap(); // Offset of _epilog from specified section
        image.write_u32::<BigEndian>(self.unresolved_offset).unwrap(); // Offset of _unresolved from specified section

        if self.rel_version >= 2 {
            image.write_u32::<BigEndian>(self.section_alignment).unwrap(); // Alignment constraint on all sections
            image.write_u32::<BigEndian>(self.bss_alignment).unwrap(); // Alignment constraint on .bss section
        }

        if self.rel_version >= 3 {
            image.write_u32::<BigEndian>(self.fixed_size).unwrap(); // Fix size
        }

        //////////////// Write section info table ////////////////
        // Null section
        debug!("Adding null section info for to REL image");
        image.write_u32::<BigEndian>(0).unwrap();
        image.write_u32::<BigEndian>(0).unwrap();

        debug!("Adding other section infos for to REL image");
        for (i, section) in self.sections.iter().enumerate() {
            let section_size = section.data.len() as u32;

            if section_size > 0 {
                let section_offset = section_offsets[&(i as u32)];
                let section_offset_div4 = section_offset / 4; // REL section offsets are divided by 4
                let mut section_offset_with_flags = section_offset_div4 << 2;
                if section.execute_flag { section_offset_with_flags += 0b01; } // Add execute bit
                if section.unk_flag { section_offset_with_flags += 0b10; } // Add unknown bit

                image.write_u32::<BigEndian>(section_offset_with_flags).unwrap(); // Add offset to section + flags
                image.write_u32::<BigEndian>(section_size).unwrap(); // Add length of section
            } else {
                // Assume .bss
                image.write_u32::<BigEndian>(0).unwrap(); // Add offset to section + flags
                image.write_u32::<BigEndian>(self.bss_size).unwrap(); // Add length of section
            }
        }

        //////////////// Write sections ////////////////
        debug!("Adding section data to REL image");
        for section in &self.sections {
            // Pad to next alignment multiple
            let cur_offset = image.len() as u32;
            let desired_offset = round_up(cur_offset, self.section_alignment);
            let padding_needed = desired_offset - cur_offset;
            for _ in 0..padding_needed { image.write_u8(0).unwrap(); }

            // Copy section data
            image.write_all(&section.data).unwrap();
        }
        // Pad to the next multiple of 4
        let cur_offset = image.len() as u32;
        let desired_offset = round_up(cur_offset, 4);
        let padding_needed = desired_offset - cur_offset;
        for _ in 0..padding_needed { image.write_u8(0).unwrap(); }

        //////////////// Write relocation table ////////////////
        debug!("Adding relocation data to REL image");
        for (_module_id, relocs) in &relocations {
            for reloc in relocs {
                image.write_u16::<BigEndian>(reloc.delta_offset).unwrap();
                image.write_u8(reloc.reloc_type).unwrap();
                image.write_u8(reloc.section).unwrap();
                image.write_u32::<BigEndian>(reloc.from_offset).unwrap();
            }
        }

        //////////////// Write imp table ////////////////
        debug!("Adding imp data to REL image");
        for (module_id, _relocs) in &relocations {
            image.write_u32::<BigEndian>(*module_id).unwrap();
            image.write_u32::<BigEndian>(relocation_table_offsets[module_id]).unwrap();
        }

        debug!("Finished creating REL image");
        image
    }

    /// We need to reorganize relocations a bit to fit in a REL image
    /// - Ordering them by to_section, then to_offset
    /// - Inserting none markers when the distance between relocations is > u16::MAX
    /// - Inserting end-of-list markers
    /// - Converting offsets to be relative from previous relocation entries, rather than all relative to the start of a
    ///   section
    fn convert_relocations_for_image(&self) -> HashMap<u32, Vec<RelRelativeReloc>> {
        let mut relocations: HashMap<u32, Vec<RelRelativeReloc>> = HashMap::with_capacity(self.relocations.len());
        for (module_id, relocs) in &self.relocations {
            let mut relocs = (*relocs).clone(); // Sorting happens in-place, and we don't want to modify the original vec
            relocs.sort_unstable_by(|a, b| { // Sort on to_section, then to_offset
                let section_ord = a.to_section.cmp(&b.to_section);
                if section_ord == Ordering::Equal {
                    return a.to_offset.cmp(&b.to_offset);
                } else {
                    return section_ord;
                }
            });

            // This will grow some more beyond this capacity
            let mut rel_relocs: Vec<RelRelativeReloc> = Vec::with_capacity(relocs.len());
            let mut cur_section: Option<u8> = None;
            let mut cur_offset: u32 = 0;
            for reloc in relocs {
                // Check if we need to change section
                let need_change_section = match cur_section {
                    Some(cur_section) => reloc.to_section != cur_section,
                    None => true,
                };
                if need_change_section {
                    // +1 to account for the first null section
                    rel_relocs.push(RelRelativeReloc::new_section_change(reloc.to_section));
                    cur_section = Some(reloc.to_section);
                    cur_offset = 0;
                }

                // If the next relocation is more than u16::MAX bytes away, we need to insert some no-operation relocs
                // to bridge the gap
                let mut to_delta_offset: u32 = reloc.to_offset - cur_offset;
                cur_offset += to_delta_offset;
                while to_delta_offset > std::u16::MAX as u32 {
                    rel_relocs.push(RelRelativeReloc::new_none(std::u16::MAX));
                    to_delta_offset -= std::u16::MAX as u32;
                }

                // Now we add the actual relocation
                rel_relocs.push(RelRelativeReloc {
                    delta_offset: to_delta_offset as u16,
                    section: reloc.from_section,
                    from_offset: reloc.from_offset,
                    reloc_type: reloc.reloc_type,
                });
            }
            // End the relocation list
            rel_relocs.push(RelRelativeReloc::new_end_of_list());

            relocations.insert(*module_id, rel_relocs);
        }

        relocations
    }
}

#[derive(Debug)]
pub struct RelSectionOffsetted {
    pub section_idx: u8,
    pub offset: u32,
}

/// A section in the REL
pub struct RelSection {
    /// The binary data for the section
    pub data: Vec<u8>,

    /// An unknown flag
    pub unk_flag: bool,

    /// Whether the section's data should be executable
    pub execute_flag: bool,
}

impl RelSection {
    pub fn from_elf_section(elf_section: &SectionHeader, elf_contents: &[u8]) -> RelSection {
        let section_start = elf_section.sh_offset as usize;
        let section_end = section_start + elf_section.sh_size as usize;

        RelSection {
            data: Vec::from(&elf_contents[section_start..section_end]),
            unk_flag: false,
            execute_flag: elf_section.is_executable(),
        }
    }
}

/// Information about a relocation for a REL. This struct does *not* mimic relocation data stored in a REL image such
/// that sorting and what not operations don't need to be performed when adding relocations to a REL. Relocations are
/// only converted to a REL compatible format on REL image generation.
#[derive(Clone)]
pub struct RelReloc {
    /// The section in this REL that we want to patch
    pub to_section: u8,

    /// The offset from the beginning of `to_section` that we want to patch
    pub to_offset: u32,

    /// The section of the symbol we want to relocate against
    pub from_section: u8,

    /// The offset from the start of `from_section` to the symbol we want to relocate against
    pub from_offset: u32,

    /// The type of relocation
    pub reloc_type: u8,
}

/// An enum of the various supported symbol kinds in a symbol table
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum SymbolType {
    /// A code object symbol type
    Function,

    /// A data object symbol type (like a variable, for example)
    Object,
}

/// An enum of symbol visibility states - only global symbols will be written out to a symbol map
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum SymbolVisibility {
    /// The symbol can link with outside code objects
    Global,

    /// The symbol cannot link with outside code objects
    Local,
}

/// A symbol entry for a module. REL images themselves don't contain a symbol table, this is just here to help the
/// linker.
#[derive(Clone)]
pub struct RelSymbol {
    /// The name of the symbol
    pub name: String,

    /// The section index that the symbol resides in
    pub section: u8,

    /// Where the symbol is, relative to the section start
    pub offset: u32,

    /// What kind of symbol this is
    pub symbol_type: SymbolType,

    /// The symbol's visibility
    pub visibility: SymbolVisibility,

    /// Where the symbol was defined in source code
    pub source_loc: SourceLoc,
}

/// A relocation struct that mimics the relocation data in a REL image
struct RelRelativeReloc {
    /// The offset from the previous relocation to this relocation. If this is the first relocation in the section, this
    /// is relative to the section start.
    pub delta_offset: u16,

    /// Usually the section of the symbol to relocate against. If `reloc_type` is 202 (`R_DOLPHIN_SECTION` or
    /// `R_RVL_SECT`), then this is instead the section in this REL image to apply following relocations to.
    pub section: u8,

    /// Offset of the symbol to relocate against, relative to the start of its section. For `module_id` 0 (aka
    /// main.dol), this is instead an absolute address.
    pub from_offset: u32,

    /// The type of the relocation
    pub reloc_type: u8,
}

impl RelRelativeReloc {
    /// Create a new relocation entry that changes the section to apply further relocations to
    fn new_section_change(section: u8) -> RelRelativeReloc {
        RelRelativeReloc {
            delta_offset: 0,
            section,
            from_offset: 0,
            reloc_type: R_DOLPHIN_SECTION,
        }
    }

    /// Create a new relocation entry that marks the end of a relocation list
    fn new_end_of_list() -> RelRelativeReloc {
        RelRelativeReloc {
            delta_offset: 0,
            section: 0,
            from_offset: 0,
            reloc_type: R_DOLPHIN_END,
        }
    }

    /// Create a new relocation entry that performs no action, useful for bridging relocation gaps that are > u16::MAX
    /// bytes across
    fn new_none(delta_offset: u16) -> RelRelativeReloc {
        RelRelativeReloc {
            delta_offset,
            section: 0,
            from_offset: 0,
            reloc_type: R_PPC_NONE,
        }
    }
}

fn round_up(num_to_round: u32, multiple: u32) -> u32
{
    if multiple == 0 { return num_to_round; }

    let remainder: u32 = num_to_round % multiple;
    if remainder == 0 { return num_to_round; }

    return num_to_round + multiple - remainder;
}
