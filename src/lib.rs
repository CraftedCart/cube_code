use std::path::PathBuf;
use std::fmt;

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate log;

pub mod link;
pub mod rel;

#[derive(Clone)]
pub enum SourceLoc {
    File(PathBuf),
    CommonSym,
}

impl fmt::Display for SourceLoc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SourceLoc::File(path) => write!(f, "'{}'", path.display()),
            SourceLoc::CommonSym => write!(f, "[common symbol]"),
        }
    }
}

pub mod error {
    error_chain! {
        foreign_links {
            Io(std::io::Error);
            BadObjectFile(goblin::error::Error);
            Serialization(serde_json::error::Error);
        }
    }
}
